" Chinese
language messages zh_CN.utf-8
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,chinese

" Vundle Need
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
Bundle 'gmarik/vundle'

" Color
Bundle 'altercation/vim-colors-solarized'
let g:solarized_termtrans=1
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"
colorscheme solarized

" jquery
Bundle 'nono/jquery.vim'

" bracket
Bundle 'Raimondi/delimitMate'

" python
Bundle 'hdima/python-syntax'

" comment use `x else `c
Bundle 'EnhCommentify.vim'

" code
Bundle 'Valloric/YouCompleteMe'
Bundle 'Lokaltog/vim-powerline'

" NerdTree use <F2>
Bundle 'scrooloose/nerdtree'
let NERDTreeWinPos='right'
let NERDTreeWinSize=31
let NERDTreeChDirMode=1
map <F2> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" Taglist  use <F3>
Bundle 'taglist.vim'
let Tlist_Auto_Update=1
let Tlist_Auto_Open = 0
let Tlist_Use_Right_Window=1
let Tlist_Show_One_File=0
let Tlist_File_Fold_Auto_Close=0
let Tlist_Exit_OnlyWindow=1
map <F3> :TlistToggle<CR>

" HTML  use <c-e> <c-n> <c-p>
Bundle 'mattn/emmet-vim'
let g:user_emmet_expandabbr_key='<c-e>'
let g:user_emmet_complete_tag=1
let g:user_emmet_next_key='<c-n>'
let g:user_emmet_prev_key='<c-p>'

" Doxygen use <F6-u> <F6-n> <F6-k> <F6-o>
Bundle 'DoxygenToolkit.vim'
let g:DoxygenToolkit_authorName="\twengcc@ihep.ac.cn"
let g:DoxygenToolkit_briefTag_pre = "@brief\t"
let g:DoxygenToolkit_dateTag = "@day\t"
let g:DoxygenToolkit_versionTag= "@version \t"
let g:DoxygenToolkit_paramTag_pre = "@param\t"
let g:DoxygenToolkit_returnTag = "@return\t"
let g:DoxygenToolkit_maxFunctionProtoLines = 30
let g:DoxygenToolkit_briefTag_funcName="yes"
let g:DoxygenToolkit_commentType="C++"
let g:doxygen_enhanced_color=1
map <F6>u :DoxAuthor<cr>
map <F6>n :Dox<cr>
map <F6>k :DoxBlock<cr>
map <F6>o O/** */<Left><Left>

" tab use <C-i>
:nn <M-1> 1gt
:nn <M-2> 2gt
:nn <M-3> 3gt
:nn <M-4> 4gt
:nn <M-5> 5gt
:nn <M-6> 6gt
:nn <M-7> 7gt
:nn <M-8> 8gt
:nn <M-9> 9gt
:nn <M-0> :tablast<CR>
nnoremap <C-i> :tabc<CR>
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4
set smarttab

"code fold
set foldmethod=manual
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc':'zo')<CR>

"move code whole line
nnoremap <C-k>  mz:m-2<cr>`z==
nnoremap <C-j>  mz:m+<cr>`z==
xnoremap <C-k>  :m'<-2<cr>gv=gv
xnoremap <C-j>  :m'>+<cr>gv=gv

" Compile use <F7>
map <silent> <F7> :call Compile()<cr>
func Compile()
exec "w"
if &filetype == 'c'
exec "!gcc % -g -o test.exe"
elseif &filetype == 'cpp'
exec "!g++ -Wall -Wfloat-equal -Wshadow -Wconversion -Winline % -g -o test.exe"
elseif &filetype == 'java'
exec "!javac %"
endif
endfunc

" Run use <F5>
map <silent> <F5> :call Run()<cr>
func Run()
if &filetype == 'python'
exec "w"
exec "!python %"
elseif &filetype =='java'
exec "w"
exec "!java %<"
elseif &filetype == 'cpp'
exec "w"
exec "!./test.exe"
elseif &filetype == 'c'
exec "w"
exec "!./test.exe"
endif
endfunc

" default
set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin
set nobackup
set shortmess=atI
autocmd BufNewFile,BufRead *.tex set filetype=tex
set nowrap
filetype plugin on
filetype indent on
filetype plugin indent on
filetype on
:set guioptions-=T
set hlsearch
set showmatch
:set laststatus=2
set number
set autoread
set showcmd
syntax enable
syntax on
set wildmenu
let mapleader = "`"
let g:mapleader = "`"
let g:C_MapLeader  = '`'
nmap he <ESC>:%!xxd<cr>
nmap hx <ESC>:%!xxd -r <cr>
:set guioptions-=m
